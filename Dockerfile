FROM python:3.6-alpine3.8
RUN adduser -D daftbot

WORKDIR /opt

RUN apk update \
    && apk add --virtual build-deps \
    gcc \
    python3-dev \
    musl-dev \
    libffi-dev \
    && apk add postgresql-dev \
    && apk add git \
    && apk add jpeg-dev \
    && apk add zlib-dev \
    && pip install --no-cache-dir psycopg2 \
    && pip install --no-cache-dir pymysql 

RUN pip install --no-cache-dir --upgrade pip
RUN pip install --no-cache-dir gunicorn
# RUN pip install --no-cache-dir numpy

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
# 


COPY run.py config.py celery_worker.py boot.sh requirements.txt ./

# CRON SECTION
# COPY cron/crontab.txt /crontab.txt
ADD cron/1min.sh cron/crontab.txt ./
# RUN chmod +x /1min.sh
# RUN /usr/bin/crontab /crontab.txt
# CRON SECTION

RUN pip install --upgrade --no-cache-dir git+https://github.com/madpin/bitoolbox.git@master

RUN pip install --upgrade git+https://github.com/madpin/daftlistings.git@dev

RUN apk del build-deps

ENV FLASK_APP run.py

RUN chmod +x boot.sh
RUN chmod +x 1min.sh
RUN chown -R daftbot:daftbot ./
RUN /usr/bin/crontab ./crontab.txt
USER daftbot

EXPOSE 5000
ENTRYPOINT ["sh", "./boot.sh"]
