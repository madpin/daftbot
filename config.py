import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    LISTINGS_PER_PAGE = 10

    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-ever-ever-guess'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
            'sqlite:///' + os.path.join(basedir, 'daftbot', 'db.sqlite')
    
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    MAIL_SERVER = os.environ.get('MAIL_SERVER')
    MAIL_PORT = int(os.environ.get('MAIL_PORT') or 587)
    MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS') is not None
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
    ADMINS = ['madpin@gmail.com']
    CELERY_BROKER_URL = 'redis://redis:6379/0'
    CELERY_RESULT_BACKEND = 'redis://redis:6379/0'
#     CELERY_TASK_SERIALIZER = 'pickle'
    CELERY_ACCEPT_CONTENT = ['json', 'pickle']
    
    HERE_APP_ID = os.environ.get('HERE_APP_ID')
    HERE_APP_CODE = os.environ.get('HERE_APP_CODE')

    # Telegram (TG) Config
    TG_BOT_TOKEN = os.environ.get('TG_BOT_TOKEN')
    TG_ADMIN_CHAT_ID = os.environ.get('TG_ADMIN_CHAT_ID')
    TG_GROUP_CHAT_ID = os.environ.get('TG_GROUP_CHAT_ID')