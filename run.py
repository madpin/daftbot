from daftbot import create_app, db
from daftbot.models import User
from daftbot.cli import bot_cli

# print('heeey')
app = create_app()
app.cli.add_command(bot_cli)

@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': User,}
