
from flask import current_app
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
import datetime
import enum
from hashlib import md5
from time import time
import jwt

from daftbot import db, login


listing_image_table = db.Table(
    'listing_image',
    db.Column('id', db.Integer, primary_key=True),
    db.Column('created_at', db.DateTime, default=datetime.datetime.now),
    db.Column('updated_at', db.DateTime, default=datetime.datetime.now,
              onupdate=datetime.datetime.now),
    db.Column('listing_id', db.Integer,
              db.ForeignKey('listing.id'), index=True),
    db.Column('image_id', db.String(32), db.ForeignKey('image.id'), index=True)
)

listing_facility_table = db.Table(
    'listing_facility',
    db.Column('id', db.Integer, primary_key=True),
    db.Column('created_at', db.DateTime, default=datetime.datetime.now),
    db.Column('updated_at', db.DateTime, default=datetime.datetime.now,
              onupdate=datetime.datetime.now),
    db.Column('listing_id', db.Integer,
              db.ForeignKey('listing.id'), index=True),
    db.Column('facility_id', db.Integer,
              db.ForeignKey('facility.id'), index=True)
)

listing_overview_table = db.Table(
    'listing_overview',
    db.Column('id', db.Integer, primary_key=True),
    db.Column('created_at', db.DateTime, default=datetime.datetime.now),
    db.Column('updated_at', db.DateTime, default=datetime.datetime.now,
              onupdate=datetime.datetime.now),
    db.Column('listing_id', db.Integer,
              db.ForeignKey('listing.id'), index=True),
    db.Column('overview_id', db.Integer,
              db.ForeignKey('overview.id'), index=True)
)

listing_pub_trans_table = db.Table(
    'listing_pub_trans',
    db.Column('id', db.Integer, primary_key=True),
    db.Column('created_at', db.DateTime, default=datetime.datetime.now),
    db.Column('updated_at', db.DateTime, default=datetime.datetime.now,
              onupdate=datetime.datetime.now),
    db.Column('listing_id', db.Integer,
              db.ForeignKey('listing.id'), index=True),
    db.Column('pub_trans_id', db.Integer, db.ForeignKey(
        'public_transport.id'), index=True)
)

routetoindeed_pub_trans_table = db.Table(
    'routetoindeed_pub_trans',
    db.Column('id', db.Integer, primary_key=True),
    db.Column('created_at', db.DateTime, default=datetime.datetime.now),
    db.Column('updated_at', db.DateTime, default=datetime.datetime.now,
              onupdate=datetime.datetime.now),
    db.Column('pub_trans_id', db.Integer, db.ForeignKey(
        'public_transport.id'), index=True),
    db.Column('route_to_indeed_id', db.String(40), db.ForeignKey(
        'route_to_indeed.id'), index=True),

    # db.Column('latitude', db.Integer, ),
    # db.Column('longitude', db.Integer, ),
    # db.ForeignKeyConstraint(['latitude', 'longitude'],
    #                         ['route_to_indeed.latitude',
    #                             'route_to_indeed.longitude']),
)


class Listing(db.Model):
    __tablename__ = 'listing'
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.datetime.now,
                           onupdate=datetime.datetime.now)
    source_id = db.Column(db.Integer)
    source_url = db.Column(db.String(500))
    source_agent_id = db.Column(db.Integer)
    raw_price = db.Column(db.String(255))
    price = db.Column(db.Integer)
    raw_address_full = db.Column(db.String(255))
    raw_town = db.Column(db.String(255))
    raw_county = db.Column(db.String(255))
    raw_agent = db.Column(db.String(255))
    raw_agent_url = db.Column(db.String(500))
    raw_contact_number = db.Column(db.String(255))
    raw_search_type = db.Column(db.String(255))
    raw_dwelling_type = db.Column(db.String(255))
    raw_area_size = db.Column(db.String(500))
    listing_date = db.Column(db.DateTime())
    date_insert_update = db.Column(db.DateTime())
    city_center_distance = db.Column(db.Numeric(10, 2))
    indeed_distance = db.Column(db.Numeric(10, 2))
    bedrooms = db.Column(db.Integer)
    bathrooms = db.Column(db.Integer)
    views = db.Column(db.Integer)
    here_processed = db.Column(db.Integer, default=0)
    shortcode = db.Column(db.String(255))
    source_xml_url = db.Column(db.String(255))
    source_xml_pub_date = db.Column(db.DateTime())
    city_center_distance = db.Column(db.Numeric(10, 2))
    latitude = db.Column(db.Numeric(8, 5))
    longitude = db.Column(db.Numeric(8, 5))
    telegram_sent = db.Column(db.Integer, default=0)
    description_id = db.Column(db.String(32), db.ForeignKey(
        'listing_description.id'))
    description = db.relationship("ListingDescription")
    images = db.relationship("Image", secondary=listing_image_table)
    facilities = db.relationship(
        "Facility", secondary=listing_facility_table)
    overviews = db.relationship(
        "Overview", secondary=listing_overview_table)
    pub_trans = db.relationship(
        "PubTrans", secondary=listing_pub_trans_table)

    address = db.relationship(
        "Address",
        foreign_keys=[latitude, longitude],
        primaryjoin="and_(Listing.latitude==Address.latitude, "
        "Listing.longitude==Address.longitude)")

    routes_to_indeed = db.relationship(
        "RouteToIndeed",
        foreign_keys=[latitude, longitude],
        primaryjoin="and_(Listing.latitude==RouteToIndeed.latitude, "
        "Listing.longitude==RouteToIndeed.longitude)",
        viewonly=True, uselist=True)

    # __table_args__ = (
    #     db.ForeignKeyConstraint(['latitude', 'longitude'],
    #                             ['address.latitude', 'address.longitude']), {})

    def __repr__(self):
        return "<Listing(id='%s', source_url='%s')>" % (self.id, self.source_url)


class RouteToIndeed(db.Model):
    __tablename__ = 'route_to_indeed'
    # id = db.Column(db.Integer, primary_key=True)
    id = db.Column(db.String(40), primary_key=True)
    latitude = db.Column(db.Numeric(8, 5),)
    longitude = db.Column(db.Numeric(8, 5),)
    created_at = db.Column(db.DateTime, default=datetime.datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.datetime.now,
                           onupdate=datetime.datetime.now)
    description = db.Column(db.Text)
    here_map_version = db.Column(db.String(20))
    here_route_type = db.Column(db.String(20))
    weekday = db.Column(db.String(20))

    day_time = db.Column(db.String(10))

    total_length = db.Column(db.Numeric(8, 3))
    total_travel_time = db.Column(db.Numeric(8, 3))
    walking_length = db.Column(db.Numeric(8, 3))
    walking_travel_time = db.Column(db.Numeric(8, 3))
    lines_count = db.Column(db.Integer)
    lines_names_concat = db.Column(db.String(255))
    less_wl = db.Column(db.Boolean, default=0)
    less_tl = db.Column(db.Boolean, default=0)

    pub_trans = db.relationship(
        "PubTrans", secondary=routetoindeed_pub_trans_table)

    # address = db.relationship(
    #     "Address",
    #     foreign_keys=[latitude, longitude],
    #     primaryjoin="and_(RouteToIndeed.latitude==Address.latitude, "
    #     "RouteToIndeed.longitude==Address.longitude)")

    # __table_args__ = (
    #     db.ForeignKeyConstraint(
    #         ['latitude', 'longitude'],
    #         ['listing.latitude', 'listing.longitude']
    #     ),
    # )

    def __repr__(self):
        return "<RouteToIndeed(lat='%s', lon='%s')>" % (self.latitude, self.longitude)


class ListingDescription(db.Model):
    __tablename__ = 'listing_description'
    id = db.Column(db.String(32), primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.datetime.now)
    description = db.Column(db.Text)
    listing = db.relationship("Listing")

    def __repr__(self):
        return "<ListingDescription(id='%s')>" % (self.id)


class ListingRaw(db.Model):
    __tablename__ = 'listing_raw'
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.datetime.now,
                           onupdate=datetime.datetime.now)
    # listing_id = db.Column(db.Integer, db.ForeignKey('listing.id'), index=True)
    source_id = db.Column(db.Integer)
    source_url = db.Column(db.String(501))
    raw = db.Column(db.Text)
    # listing = db.relationship("Listing")

    def __repr__(self):
        return "<ListingRaw(id='%s', source_url='%s')>" % (self.id, self.source_url)


class Facility(db.Model):
    __tablename__ = 'facility'
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.datetime.now,
                           onupdate=datetime.datetime.now)
    name = db.Column(db.String(255))
    desc = db.Column(db.String(255))
    listings = db.relationship(
        "Listing", secondary=listing_facility_table)

    def __repr__(self):
        return "<Facility(id='%s', name='%s')>" % (self.id, self.name)


class Overview(db.Model):
    __tablename__ = 'overview'
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.datetime.now,
                           onupdate=datetime.datetime.now)
    name = db.Column(db.String(500))
    short_name = db.Column(db.String(255))
    listings = db.relationship(
        "Listing", secondary=listing_overview_table)

    def __repr__(self):
        return "<Overview(id='%s', short_name='%s')>" % (self.id, self.short_name)


class Address(db.Model):
    __tablename__ = 'address'
    latitude = db.Column(db.Numeric(8, 5), primary_key=True)
    longitude = db.Column(db.Numeric(8, 5), primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.datetime.now,
                           onupdate=datetime.datetime.now)
    label = db.Column(db.Text)
    country = db.Column(db.String(255))
    county = db.Column(db.String(255))
    district = db.Column(db.String(255))
    street = db.Column(db.String(500))
    postalcode = db.Column(db.String(255))
    housenumber = db.Column(db.Integer)
    # listings = db.relationship("Listing",
    #                            primaryjoin="and_(Listing.latitude==Address.latitude, "
    #                            "Listing.longitude==Address.longitude)")

    def __repr__(self):
        return "<Address(lat='%s', long='%s', label='%s')>" % (self.latitude, self.longitude, self.label)


class Image(db.Model):
    __tablename__ = 'image'
    id = db.Column(db.String(32), primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.datetime.now,
                           onupdate=datetime.datetime.now)
    # listing_id = db.Column(db.Integer, db.ForeignKey('listing.id'), index=True)
    url = db.Column(db.String(500))
    filepath = db.Column(db.String(255))
    tbfilepath = db.Column(db.String(255))
    downloaded = db.Column(db.Integer, default=0)
    rekognition_done = db.Column(db.Integer, default=0)
    tf_done = db.Column(db.Integer, default=0)
    listings = db.relationship(
        "Listing", secondary=listing_image_table)
    image_tags = db.relationship("ImageTag")

    def __repr__(self):
        return "<Image(id='%s', url='%s')>" % (self.id, self.url)


class TagSource(enum.Enum):
    rekognition = 1
    tf_inception = 2


class ImageTag(db.Model):
    __tablename__ = 'image_tag'
    # id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.datetime.now,
                           onupdate=datetime.datetime.now)
    image_id = db.Column(db.String(32), db.ForeignKey(
        'image.id'), primary_key=True)
    tag_id = db.Column(db.Integer, db.ForeignKey('tag.id'), primary_key=True)
    confidence = db.Column(db.Numeric(10, 5))
    tag_source = db.Column(db.Enum(TagSource))
    tag = db.relationship("Tag")
    image = db.relationship("Image")

    def __repr__(self):
        return "<ImageTag(image_id='%s', tag_id='%s')>" % (self.image_id, self.tag_id)


class Tag(db.Model):
    __tablename__ = 'tag'
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.datetime.now,
                           onupdate=datetime.datetime.now)
    tag = db.Column(db.String(255))
    images_tag = db.relationship("ImageTag")

    def __repr__(self):
        return "<Tag(id='%s', tag='%s')>" % (self.id, self.tag)


class PubTransTypes(enum.Enum):
    bus = 1
    dart = 2
    luas = 3
    tram = 4
    train = 5


class PubTrans(db.Model):
    __tablename__ = 'public_transport'
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.datetime.now,
                           onupdate=datetime.datetime.now)
    pub_trans_name = db.Column(db.String(255))
    pub_trans_type = db.Column(db.Enum(PubTransTypes))
    # pub_trans_type = db.Column(db.String(255))
    pub_trans_code = db.Column(db.String(255))
    daft_id = db.Column(db.String(50))
    listings = db.relationship(
        "Listing", secondary=listing_pub_trans_table)
    route_to_indeed = db.relationship(
        "RouteToIndeed", secondary=routetoindeed_pub_trans_table)

    pub_trans_alerts = db.relationship("PubTransAlert")

    def __repr__(self):
        return "<PubTrans(id='%s', name='%s')>" % (self.id, self.pub_trans_name)

# ########################################################################
# ################### Admin Data From Now ################################
# ########################################################################

# ########################################################################
# ################### Admin Data From Now ################################
# ########################################################################

# ########################################################################
# ################### Admin Data From Now ################################
# ########################################################################


class User(UserMixin, db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.datetime.now,
                           onupdate=datetime.datetime.now)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password = db.Column(db.String(255))
    password_hash = db.Column(db.String(128))
    last_seen = db.Column(db.DateTime, default=datetime.datetime.now)
    about_me = db.Column(db.String(255))

    pub_trans_alerts = db.relationship("PubTransAlert")

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)
        self.password = password

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return "<User(id='%s', email='%s')>" % (self.id, self.email)

    def avatar(self, size):
        digest = md5(self.email.lower().encode('utf-8')).hexdigest()
        return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(
            digest, size)

    def get_reset_password_token(self, expires_in=600):
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in},
            current_app.config['SECRET_KEY'], algorithm='HS256').decode('utf-8')

    @staticmethod
    def verify_reset_password_token(token):
        try:
            id = jwt.decode(token, current_app.config['SECRET_KEY'],
                            algorithms=['HS256'])['reset_password']
        except:
            return
        return User.query.get(id)


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


class PubTransAlert(db.Model):
    __tablename__ = 'public_transport_alert'
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.datetime.now,
                           onupdate=datetime.datetime.now)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), index=True)
    pub_trans_id = db.Column(db.Integer, db.ForeignKey(
        'public_transport.id'), index=True)
    current_offset = db.Column(db.Integer)
    current_finished = db.Column(db.Integer)
    last_run = db.Column(db.DateTime)
    search_source = db.Column(db.String(255))
    desc = db.Column(db.String(255))
    distance = db.Column(db.String(255))
    user = db.relationship("User")
    pub_trans = db.relationship("PubTrans")
    pub_trans_alert_log = db.relationship("PubTransAlertLog")

    def __repr__(self):
        return "<PubTransAlert(id='%s', desc='%s')>" % (self.id, self.desc)


class RSSAlert(db.Model):
    __tablename__ = 'rss_alert'
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.datetime.now,
                           onupdate=datetime.datetime.now)
    # url = db.Column(db.Integer, db.ForeignKey('user.id'), index=True)
    # pub_trans_id = db.Column(db.Integer, db.ForeignKey(
    #     'public_transport.id'), index=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), index=True)
    last_run = db.Column(db.DateTime)
    url = db.Column(db.String(255))
    type = db.Column(db.String(255))
    locations = db.Column(db.String(255))
    description = db.Column(db.String(255))
    min_price = db.Column(db.Integer)
    max_price = db.Column(db.Integer)
    current_finished = db.Column(db.Integer)
    user = db.relationship("User")

    def __repr__(self):
        return "<RSSAlert(id='%s', type='%s')>" % (self.id, self.type)


class PubTransAlertLog(db.Model):
    __tablename__ = 'public_transport_alert_log'
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.datetime.now,
                           onupdate=datetime.datetime.now)
    pub_trans_alert_id = db.Column(db.Integer, db.ForeignKey(
        'public_transport_alert.id'), index=True)
    pub_trans_alert = db.relationship("PubTransAlert")

    def __repr__(self):
        return "<PubTransAlertLog(id='%s')>" % (self.id)


class URLProcess(db.Model):
    __tablename__ = 'url_process'
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.datetime.now,
                           onupdate=datetime.datetime.now)
    url = db.Column(db.String(500))
    type = db.Column(db.String(255))
    times = db.Column(db.Integer)


class CeleryTask(db.Model):
    __tablename__ = 'celery_task'
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.datetime.now,
                           onupdate=datetime.datetime.now)
    task_id = db.Column(db.String(40))
    status = db.Column(db.String(255))
    state = db.Column(db.String(255))
    result = db.Column(db.Integer)
