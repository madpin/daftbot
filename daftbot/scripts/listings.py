# -*- coding: utf-8 -*-
import daftlistings as daft
from daftbot.models import URLProcess
from daftbot import db


def set_url_process(url):
    NewURLProcess = URLProcess()
    NewURLProcess.url = url
    NewURLProcess.type = 'search'
    db.session.add(NewURLProcess)
    db.session.commit()


def get_sharing_listings(pub_trans_daft_id,
                         offset=0,
                         min_price=100,
                         max_price=1500,
                         with_pics=True,
                         ):
    daft_search = daft.Daft()
    daft_search.set_offset(offset)
    daft_search.set_public_transport_route(pub_trans_daft_id)

    daft_search.set_county('Dublin City')

    daft_search.set_min_price(min_price)
    daft_search.set_max_price(max_price)
    daft_search.set_availability(5)
    daft_search.set_gender(daft.Gender.MALE)
    daft_search.set_room_type(daft.RoomType.SINGLE_OR_DOUBLE)
    daft_search.set_with_photos(with_pics)
    daft_search.set_listing_type(daft.RentType.ROOMS_TO_SHARE)
    daft_search.set_area_type(daft.AreaType.TRANSPORT_ROUTE)

    listings = daft_search.search()
    set_url_process(daft_search.get_url())
    return listings


def get_renting_listings(pub_trans_daft_id,
                         offset=0,
                         min_price=100,
                         max_price=1500,
                         ):
    daft_search = daft.Daft()
    daft_search.set_offset(offset)
    daft_search.set_county('Dublin City')

    daft_search.set_min_price(min_price)
    daft_search.set_max_price(max_price)
    daft_search.set_listing_type(daft.RentType.ANY)
    daft_search.set_property_type([
        daft.PropertyType.APARTMENT,
        daft.PropertyType.HOUSE,
        daft.PropertyType.STUDIO,
        daft.PropertyType.FLAT,
    ])
    daft_search.set_area_type(daft.AreaType.TRANSPORT_ROUTE)
    daft_search.set_public_transport_route(pub_trans_daft_id)

    listings = daft_search.search()
    set_url_process(daft_search.get_url())

    return listings


from daftbot.models import PubTransAlert
from daftbot.scripts.daft2db import daft2db
from datetime import datetime
from flask import current_app
from daftbot import db, celery
import celery


def loop_new_listings(listing_type='sharing', max_listing_pages=9999):
    ret_tasks = []

    pub_trans_alerts = db.session.query(
        PubTransAlert
    ).filter(
        PubTransAlert.pub_trans_id.isnot(None)
    ).order_by(
        PubTransAlert.last_run.desc()
    ).order_by(
        PubTransAlert.current_finished.asc()
    ).all()

    current_listing_page = 0

    for pub_trans_alert in pub_trans_alerts:
        offset = 0
        if(pub_trans_alert.current_finished != 1):
            offset = (pub_trans_alert.current_offset or 0)+1
        pub_trans_alert.last_run = datetime.now()
        pub_trans_alert.current_finished = 0
        while True:
            listings = None
            if(listing_type == 'sharing'):
                listings = get_sharing_listings(
                    pub_trans_alert.pub_trans.daft_id,
                    offset,
                )
            elif(listing_type == 'renting'):
                listings = get_renting_listings(
                    pub_trans_alert.pub_trans_daft_id,
                    offset,
                )

            for listing in listings:
                task = daft2db.delay(
                    str(listing._data_from_search),
                    None
                )
                ret_tasks.append(str(task))

            pub_trans_alert.current_offset = offset
            offset = offset + len(listings)
            current_listing_page += 1
            if len(listings) <= 0:
                pub_trans_alert.current_finished = 1
                break
            if current_listing_page >= max_listing_pages:
                break
        if current_listing_page >= max_listing_pages:
            break
    db.session.commit()
    return ret_tasks


from daftbot.models import RSSAlert, Listing
import requests
from bs4 import BeautifulSoup
from datetime import datetime

@celery.task(autoretry_for=(Exception,),
             max_retries=0,
             serializer='pickle')
def loop_xml_listings():

    _headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.0; WOW64; rv:24.0) Gecko/20100101 Firefox/24.0'}
    ret_tasks = []

    xml_alerts = db.session.query(
        RSSAlert
    ).order_by(
        RSSAlert.last_run.desc()
    ).order_by(
        RSSAlert.current_finished.asc()
    ).all()

    current_listing_page = 0

    for xml_alert in xml_alerts:
        xml_alert.last_run = datetime.now()
        xml_alert.current_finished = 0

        # daft_search = daft.Daft()
        # daft_search.set_xml_url(xml_alert.url)
        # listings = daft_search.read_xml()

        urls_to_process = []

        req = requests.get(xml_alert.url, headers=_headers)
        if req.status_code != 200:
            continue
        soup = BeautifulSoup(req.content, 'html.parser')
        divs = soup.find_all("item")
        for div in divs:
            urls_to_process.append({
                'url': div.find('guid').text,
                'pub_date': datetime.strptime(div.find('pubdate').text, '%a, %d %b %Y %H:%M:%S %z').replace(tzinfo=None),
            })

        listings_in_db = db.session.query(Listing).filter(
            Listing.source_xml_url.in_([_['url'] for _ in urls_to_process])
        ).all()

        # Removing unwanted listings
        for listing_in_db in listings_in_db:
            dburl = listing_in_db.source_xml_url
            pub_date = [_.get('pub_date') for _ in urls_to_process if _.get('url') == dburl][0]
            if(listing_in_db.source_xml_pub_date and
               pub_date <= listing_in_db.source_xml_pub_date):
                urls_to_process[:] = [d for d in urls_to_process
                                      if d.get('url') != listing_in_db.source_xml_url]
        if(urls_to_process):
            current_app.logger.info(urls_to_process)

        for url_to_process in urls_to_process:
            task = daft2db.delay(
                listing_url=url_to_process['url'],
                source_xml_pub_date = url_to_process['pub_date'],
            )
            ret_tasks.append(str(task))
    db.session.commit()
    return ret_tasks
