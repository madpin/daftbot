# -*- coding: utf-8 -*-

import requests
import json
import hashlib
import datetime

from flask import current_app
from daftbot import db, celery
from daftbot.models import Listing, RouteToIndeed, PubTrans
from sqlalchemy import update

from bitoolbox import geotools


@celery.task(autoretry_for=(Exception,),
             max_retries=0,
             serializer='pickle')
def loop_here():
    ret_tasks = []
    today = datetime.date.today()
    to_friday = 5+7-today.isoweekday()
    next2_friday = today + datetime.timedelta(days=to_friday)
    complete_date = datetime.datetime.combine(next2_friday,
                                            datetime.time(8, 45))

    listings = db.session.query(Listing).filter_by(
        here_processed=0
    ).order_by(
        Listing.date_insert_update.asc()
    ).all()

    current_listing_page = 0

    for listing in listings:
        p1 = (listing.latitude, listing.longitude)
        task = route2db.delay(complete_date, p1)
        ret_tasks.append(str(task))
    return ret_tasks


@celery.task(autoretry_for=(Exception,),
             max_retries=0,
             serializer='pickle')
def route2db(complete_date, p1):
    
    with current_app.app_context():
        today = datetime.date.today()
        to_friday = 5+7-today.isoweekday()
        next2_friday = today + datetime.timedelta(days=to_friday)
        complete_date = datetime.datetime.combine(next2_friday,
                                                datetime.time(8, 45))

        week_days = ("Segunda", "Tuesday", "Wednesday",
                    "Thursday", "Friday", "Saturday", "Sunday")

        list_route = get_routes_to_indeed(complete_date, p1)
        # print(list_route)

        if('routes' not in list_route):
            current_app.logger.warning(list_route)
        else:
            min_tl = min([x['total_length'] for x in list_route['routes']])
            min_wl = min([x['walking_length'] for x in list_route['routes']])

            for route in list_route['routes']:
                dbroute = RouteToIndeed()
                dbroute.id = str(route['hash'])
                dbroute.latitude = str(list_route['p1_latitude'])
                dbroute.longitude = str(list_route['p1_longitude'])
                dbroute.here_map_version = list_route['map_version']

                dbroute.here_route_type = route['here_route_type']

                dbroute.weekday = week_days[complete_date.weekday()]
                dbroute.day_time = complete_date.strftime("%H:%M:%S")

                dbroute.total_length = route['total_length']
                dbroute.total_travel_time = route['total_travel_time']

                dbroute.walking_length = route['walking_length']
                dbroute.walking_travel_time = route['walking_travel_time']
                dbroute.lines_count = route['lines_count']

                if(route['total_length'] == min_tl):
                    dbroute.less_tl = 1
                if(route['walking_length'] == min_wl):
                    dbroute.less_wl = 1
                
                lines_names_str = ''
                for line in route['lines']:
                    lines_names_str += line['line_name'] + ', '
                    cur_pub_trans = db.session.query(PubTrans).filter(
                        PubTrans.pub_trans_name.like(line['line_name'])
                    ).first()
                    if(cur_pub_trans):
                        dbroute.pub_trans.append(cur_pub_trans)
                    else:
                        current_app.logger.info(
                            'Route do not Exists: ' + line['line_name'])

                dbroute.lines_names_concat = lines_names_str
                db.session.merge(dbroute)
            
        stmt = update(Listing).\
            where(Listing.latitude == p1[0]).\
            where(Listing.longitude == p1[1]).\
            values(here_processed='1')
        db.session.execute(stmt)
        db.session.commit()
    return 0


def get_routes_to_indeed(route_date, p1, p2=(53.339224, -6.261361)):
    p1_str = str(p1[0])+','+str(p1[1])
    p2_str = str(p2[0])+','+str(p2[1])
    ret = {}
    url = 'https://route.api.here.com/routing/7.2/calculateroute.json'

    payload = {
        'app_id': current_app.config['HERE_APP_ID'],
        'app_code': current_app.config['HERE_APP_CODE'],
        'waypoint0': p1_str,
        'waypoint1': p2_str,  # Indeed Address
        'mode': 'fastest;publicTransportTimeTable',
        'combineChange': 'true',
        'arrival': route_date.strftime("%Y-%m-%dT%H:%M:%S"),
        'alternatives': '9',
    }

    response = requests.get(url, params=payload)
    response.encoding = 'utf-8'

    if(response.ok):
        # print(response.content)
        json_data = json.loads(response.content)
    #     print(json_data)
    elif(response.status_code == 400):
        json_data = json.loads(response.content)
        return json_data
    else:
        # If response code is not ok (200), print the resulting http error code with description
        current_app.error(response.content)
        response.raise_for_status()

    ret['p1_latitude'] = p1[0]
    ret['p1_longitude'] = p1[1]
    ret['p2_latitude'] = p2[0]
    ret['p2_longitude'] = p2[1]
    dict_header_str = str(ret)
    ret['routes'] = []
    for route in json_data['response']['route']:
        ret_route = {}
        ret_route['here_route_type'] = route['mode']['type']
        ret_route['total_length'] = route['summary']['distance']/1000
        ret_route['total_travel_time'] = round(
            route['summary']['travelTime']/60, 2)

        ret_route['lines'] = []
        for line in route['publicTransportLine']:
            ret_line = {}
            ret_line['line_name'] = line['lineName'].upper()

            if('GREEN' in ret_line['line_name']):
                ret_line['line_name'] = 'Green Line'
            elif('RED' in ret_line['line_name']):
                ret_line['line_name'] = 'Red Line'

            ret_route['lines'].append(ret_line)

        ret_route['lines_count'] = len(ret_route['lines'])

        walking_time = 0
        walking_length = 0
        for maneuver in route['leg'][0]['maneuver']:
            if(maneuver['_type'] == 'PrivateTransportManeuverType'):
                walking_time += maneuver['travelTime']
                walking_length += maneuver['length']

        ret_route['walking_length'] = walking_length/1000
        ret_route['walking_travel_time'] = walking_time/60
        ret_route['hash'] = hashlib.md5(
            (str(ret)+dict_header_str).encode('utf-8')).hexdigest()

        ret['routes'].append(ret_route)

    ret['map_version'] = json_data['response']['metaInfo']['mapVersion']

    return ret

