
import string
import random
from datetime import timedelta

from flask import current_app, jsonify
from celery.task.base import periodic_task
from celery import chain

from daftbot import db, celery
from daftbot.models import CeleryTask


# @periodic_task(run_every=timedelta(seconds=5))
# def periodic_run_get_manifest():
#     newrec = CeleryTask(
#         id='1',
#         status=''.join(random.choices(
#             string.ascii_lowercase + string.digits, k=20
#         )),
#     )
#     db.session.merge(newrec)
#     db.session.commit()

#     rec = db.session.query(
#         CeleryTask
#     ).filter_by(
#         id=1
#     ).first()
#     return jsonify({
#         'updated': rec.updated_at.strftime('%Y-%d-%m %H:%M:%S'),
#         'status': rec.status,
#     })


@periodic_task(run_every=timedelta(seconds=30))
def update_db():
    from daftbot.scripts.listings import loop_xml_listings
    from daftbot.scripts.download_images import loop_images
    from daftbot.scripts.here import loop_here
    from daftbot.scripts.telegram import loop_telegram

    ret = {}
    # ret['loop_xml_listings'] = loop_xml_listings()
    # ret['loop_images'] = loop_images()
    # ret['loop_here'] = loop_here()

    chain(
        loop_xml_listings.si(),
        loop_images.si(),
        loop_here.si(),
        loop_telegram.si(),
    )()
    return ret
