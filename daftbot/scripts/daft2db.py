# -*- coding: utf-8 -*-
from flask import current_app
from daftbot import db, celery

from daftbot.models import *
# from sqlalchemy import and_
import json
from datetime import datetime

import re
import hashlib
import pickle

import daftlistings as daft
from bitoolbox import geotools


def NumberFromStrings(string):
    numberfromstring = re.findall('\d+', str(string))
    return ''.join(numberfromstring)


@celery.task(autoretry_for=(Exception,),
             max_retries=2,
             serializer='pickle')
def daft2db(listing_data=None, listing_url=None, pub_trans_alert=None,
            source_xml_pub_date=None):

    if(listing_data):
        listing = daft.Listing(data_from_search=listing_data)
    elif(listing_url):
        listing = daft.Listing(url=listing_url)

    data = listing.as_dict()

    listingRaw = ListingRaw()
    listingRaw.raw = json.dumps(str(data))
    listingRaw.source_url = data['daft_link']
    db.session.add(listingRaw)

    NewURLProcess = URLProcess()
    NewURLProcess.url = data['daft_link']
    NewURLProcess.type = 'listing'
    db.session.add(NewURLProcess)

    cur_listing_id = None
    cur_listing = db.session.query(Listing).filter_by(
        source_url=data['daft_link']
    ).first()
    if(cur_listing):
        cur_listing_id = cur_listing.id

    list_facilities = []
    if('facilities' in data and isinstance(data['facilities'], list)):
        for facility in data['facilities']:
            cur_facility_id = None
            cur_facility = db.session.query(
                Facility).filter_by(name=facility).first()

            if(cur_facility):
                cur_facility_id = cur_facility.id

            facility_obj = Facility(id=cur_facility_id, name=facility)

            list_facilities.append(facility_obj)

    list_overviews = []
    if('overviews' in data and isinstance(data['overviews'], list)):
        for overview in data['overviews']:
            cur_overview_id = None
            cur_overview = db.session.query(
                Overview).filter_by(name=overview).first()

            if(cur_overview):
                cur_overview_id = cur_overview.id

            overview_obj = Overview(id=cur_overview_id, name=overview)

            list_overviews.append(overview_obj)

    listing_images = []
    if('listing_hires_image' in data and isinstance(data['listing_hires_image'], list)):
        for image in data['listing_hires_image']:
            cur_image_id = hashlib.md5(image.encode('utf-8')).hexdigest()
            image_obj = Image(id=cur_image_id, url=image)
            listing_images.append(image_obj)

    listing = None
    listing = Listing(id=cur_listing_id)

    if(listing_url):
        listing.source_xml_url = listing_url

    if(source_xml_pub_date):
        listing.source_xml_pub_date = source_xml_pub_date

    if('longitude' in data
       and data['longitude']
       and len(str(data['longitude'])) >= 1
       and 'latitude' in data
       and data['latitude']
       and len(str(data['latitude'])) >= 1
       ):
        listing.indeed_distance = geotools.distance_latlon(
            (data['latitude'], data['longitude']),
            (53.339224, -6.261361)
        )
    if('ad_search_type' in data
       and data['ad_search_type']
       and len(str(data['ad_search_type'])) >= 1
       ):
        listing.raw_search_type = data['ad_search_type']

    if('agent_id' in data
       and data['agent_id']
       and len(str(data['agent_id']))
       ):
        listing.source_agent_id = data['agent_id']

    if('id' in data
       and data['id']
       and len(str(data['id'])) >= 1
       ):
        listing.source_id = data['id']

    if('price' in data
       and data['price']
       and len(str(data['price'])) >= 1
       ):
        price = 0
        listing.raw_price = data['price']
        raw_str = listing.raw_price.lower()
        price = int(''.join(c for c in raw_str if c.isdigit()))
        if('week' in raw_str):
            price = price * 4.33333
        if('from' in raw_str):
            price += 1
        listing.price = price

    if('formalised_address' in data
       and data['formalised_address']
       and len(str(data['formalised_address'])) >= 1
       ):
        listing.raw_address_full = data['formalised_address']

    if('town' in data
       and data['town']
       and len(str(data['town'])) >= 1
       ):
        listing.raw_town = data['town']

    if('county' in data
       and data['county']
       and len(str(data['county'])) >= 1
       ):
        listing.raw_county = data['county']

    if('latitude' in data
       and data['latitude']
       and len(str(data['latitude'])) >= 1
       ):
        listing.latitude = data['latitude']

    if('longitude' in data
       and data['longitude']
       and len(str(data['longitude'])) >= 1
       ):
        listing.longitude = data['longitude']

    if('city_center_distance' in data
       and data['city_center_distance']
       and len(str(data['city_center_distance'])) >= 1
       ):
        listing.city_center_distance = data['city_center_distance']

    if('agent' in data
       and data['agent']
       and len(str(data['agent'])) >= 1
       ):
        listing.raw_agent = data['agent']

    if('agent_url' in data
       and data['agent_url']
       and len(str(data['agent_url'])) >= 1
       ):
        listing.raw_agent_url = data['agent_url']

    if('contact_number' in data
       and data['contact_number']
       and len(str(data['contact_number'])) >= 1
       ):
        listing.raw_contact_number = data['contact_number']

    if('daft_link' in data
       and data['daft_link']
       and len(str(data['daft_link'])) >= 1
       ):
        listing.source_url = data['daft_link']

    if('dwelling_type' in data
       and data['dwelling_type']
       and len(str(data['dwelling_type'])) >= 1
       ):
        listing.raw_dwelling_type = data['dwelling_type']

    if('posted_since' in data
       and data['posted_since']
       and len(str(data['posted_since'])) >= 1
       ):
        date_str = data['date_insert_update']
        date_str_formated = date_str.split(
            '/')[2]+'-'+date_str.split('/')[1].zfill(2)+'-'+date_str.split('/')[0].zfill(2)
        listing.listing_date = datetime.strptime(
            date_str_formated, '%Y-%m-%d')

    if('num_bedrooms' in data
       and data['num_bedrooms']
       and len(NumberFromStrings(data['num_bedrooms'])) >= 1
       ):
        listing.bedrooms = NumberFromStrings(data['num_bedrooms'])

    if('num_bathrooms' in data
       and data['num_bathrooms']
       and len(NumberFromStrings(data['num_bathrooms'])) >= 1
       ):
        listing.bathrooms = NumberFromStrings(data['num_bathrooms'])

    if('views' in data
       and data['views']
       and len(str(data['views'])) >= 1
       ):
        listing.views = data['views']

    if('description' in data
       and data['description']
       and len(str(data['description'])) >= 1
       ):
        listing.description = ListingDescription(
            id=hashlib.md5(str(data['description']).encode(
                'utf-8')).hexdigest(),
            description=data['description']
        )

    if('shortcode' in data
       and data['shortcode']
       and len(str(data['shortcode'])) >= 1
       ):
        listing.shortcode = data['shortcode']

    if('date_insert_update' in data
       and data['date_insert_update']
       and len(str(data['date_insert_update'])) >= 1
       ):
        date_str = data['date_insert_update']
        date_str_formated = date_str.split(
            '/')[2]+'-'+date_str.split('/')[1].zfill(2)+'-'+date_str.split('/')[0].zfill(2)
        listing.date_insert_update = datetime.strptime(
            date_str_formated, '%Y-%m-%d')

    add_alert_route = True
    if('transport_routes' in data
       and isinstance(data['transport_routes'], dict)
       and len(str(data['transport_routes'])) >= 1):
        for _, pub_transs in data['transport_routes'].items():
            for pub_trans_name in pub_transs:
                pub_trans = db.session.query(PubTrans).filter_by(
                    pub_trans_name=pub_trans_name).first()
                if(pub_trans):
                    listing.pub_trans.append(pub_trans)
                    if(not pub_trans_name and pub_trans_alert.pub_trans.pub_trans_name == pub_trans_name):
                        add_alert_route = False
                else:
                    print(pub_trans_name)
    if(add_alert_route and pub_trans_alert):
        listing.pub_trans.append(pub_trans_alert.pub_trans)

    listing.facilities.extend(list_facilities)
    listing.overviews.extend(list_overviews)
    listing.images.extend(listing_images)

    db.session.merge(listing)
    db.session.commit()

    return 0
