from flask import current_app, render_template
from daftbot import db, celery

from daftbot.models import Listing

import telegram
from celery import chain



@celery.task(autoretry_for=(Exception,),
             max_retries=0,
             serializer='pickle')
def loop_telegram():
    ret = []
    params_to_chain = []
    listings = db.session.query(Listing).filter_by(
        telegram_sent=0
    ).order_by(
        Listing.date_insert_update.desc()
    ).order_by(
        Listing.source_xml_pub_date.desc().nullslast()
    ).limit(3).all()
    if(len(listings) > 0):
        for listing in listings:
            params_to_chain.append(get_message_params(listing))
            current_app.logger.info('Listing to send in Telegram: ' + listing.shortcode)
            listing.telegram_sent = 1
        db.session.commit()
        ret = chain(send_message_to.si(i) for i in params_to_chain).delay()
    return str(ret)


def get_message_params(listing):
    bot = telegram.Bot(token=current_app.config['TG_BOT_TOKEN'])

    msg = str(render_template(
        'emails/telegram_message.html',
        listing=listing
    ))

    medias_array = []
    cur_image_arrays = []
    c = 0
    for image in listing.images:
        cur_image_arrays.append(telegram.InputMediaPhoto(image.url))

        c += 1
        if(c >= 10):
            c = 0
            medias_array.append(cur_image_arrays)
            cur_image_arrays = []
    medias_array.append(cur_image_arrays)

    return {
        'msg': msg,
        'medias_array': medias_array
    }


import time


@celery.task(autoretry_for=(Exception,),
             max_retries=0,
             serializer='pickle')
def send_message_to(param_obj):

    bot = telegram.Bot(token=current_app.config['TG_BOT_TOKEN'])

    try:
        message_ret = bot.send_message(
            chat_id=current_app.config['TG_ADMIN_CHAT_ID'],
            text=param_obj['msg'],
            parse_mode=telegram.ParseMode.HTML,
            timeout=10,
            disable_web_page_preview=True,
        )
    except telegram.error.BadRequest:
        current_app.logger.error('BadRequest: Errors loading images: ' + str(param_obj['msg']))
    ret_medias = []
    for medias in param_obj['medias_array']:
        try:
            if(len(medias) > 0):
                ret_media = bot.send_media_group(
                    chat_id=current_app.config['TG_ADMIN_CHAT_ID'],
                    media=medias,
                    timeout=10,
                )
        except telegram.error.BadRequest:
            current_app.logger.error('BadRequest: Errors loading images: ' + str(medias))
        ret_medias.append(str(ret_media))
    time.sleep(1)
    return {
        'message_ret': str(message_ret),
        'ret_medias': ret_medias,
    }
