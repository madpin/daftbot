# -*- coding: utf-8 -*-

from flask import current_app
from daftbot import db, celery
from daftbot.models import Image
from PIL import Image as PILImage
import requests
import os
import pickle
from sqlalchemy import update

suffix_list = ['jpg', 'jpeg', 'gif', 'png', 'tif', 'svg', ]
url_folder_name = os.path.join('static', 'daftimages')
url_tbfolder_name = os.path.join('static', 'daftimages')
folder_name = os.path.join('daftbot', 'static', 'daftimages')
tbfolder_name = os.path.join('daftbot', 'static', 'daftimages')

tb_size = 400,400
CHECK_FILE_EXISTS = False


def download(url, fulllfile):
    # open in binary mode
    with open(fulllfile, "wb") as file:
        # get request
        response = requests.get(url)
        # write to file
        file.write(response.content)


@celery.task(autoretry_for=(Exception,),
             max_retries=0,
             serializer='pickle')
def download_image(image_id, image_url):
    file_suffix = image_url[image_url.rfind("/")+1:].split('.')[1]
    filename = image_id + '.' + file_suffix

    if not os.path.exists(os.path.join(folder_name, filename[0])):
        try:
            os.makedirs(os.path.join(folder_name, filename[0]))
        except:
            pass

    tbfilename = 'tb_' + image_id + '.' + file_suffix
    fulllfile = str(os.path.join(folder_name, filename[0], filename))
    tbfulllfile = str(os.path.join(tbfolder_name, filename[0], tbfilename))

    if(file_suffix in suffix_list):
        if(CHECK_FILE_EXISTS and os.path.isfile(fulllfile)):
            pass
            # image.downloaded = 1
        else:
            download(image_url, fulllfile)

        if(CHECK_FILE_EXISTS and os.path.isfile(tbfulllfile)):
            print('tb já existe')
        else:
            if(os.path.isfile(fulllfile)):
                im = PILImage.open(fulllfile)
                im.thumbnail(tb_size, PILImage.ANTIALIAS)
                im.save(tbfulllfile)
    stmt = update(Image).\
        where(Image.id == image_id).\
        values({'downloaded': '1',
                'filepath': str(
                    os.path.join(url_folder_name, filename[0], filename)
                ),
                'tbfilepath': str(
                    os.path.join(url_tbfolder_name, filename[0], tbfilename)
                ),
                })
    db.session.execute(stmt)
    db.session.commit()
    return '1'


@celery.task(autoretry_for=(Exception,),
             max_retries=0,
             serializer='pickle')
def loop_images():
    ret_tasks = []
    images = db.session.query(
        Image
    ).filter_by(
        downloaded=0
    ).limit(1000).all()

    for image in images:
        task = download_image.delay(image.id, image.url)
        ret_tasks.append(str(task))
    return ret_tasks