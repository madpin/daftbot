# -*- coding: utf-8 -*-
from flask import current_app
from daftbot import db, celery
from daftbot.models import User, PubTrans, PubTransAlert, RSSAlert

users = [
    {'id': '1', 'user': 'm', 'pass': 'm', 'email': 'madpin@gmail.com'},
]

viable_transs = [
    {'code': 'Green Line', 'distance': 'closest'},
    {'code': '32X', 'distance': 'close'},
    {'code': '41X', 'distance': 'close'},
    {'code': '193', 'distance': 'close'},
    {'code': '194', 'distance': 'close'},
    {'code': '824', 'distance': 'close'},
    {'code': '25X', 'distance': 'close'},
    {'code': '66x', 'distance': 'close'},
    {'code': '67X', 'distance': 'close'},
    {'code': '120', 'distance': 'close'},
    {'code': '126', 'distance': 'close'},
    {'code': '133', 'distance': 'close'},
    {'code': '9', 'distance': 'kinda-far'},
    {'code': '14', 'distance': 'kinda-far'},
    {'code': '14C', 'distance': 'kinda-far'},
    {'code': '15', 'distance': 'kinda-far'},
    {'code': '15A', 'distance': 'kinda-far'},
    {'code': '15B', 'distance': 'kinda-far'},
    {'code': '15D', 'distance': 'kinda-far'},
    {'code': '16', 'distance': 'kinda-far'},
    {'code': '49N', 'distance': 'kinda-far'},
    {'code': '65', 'distance': 'kinda-far'},
    {'code': '65B', 'distance': 'kinda-far'},
    {'code': '68', 'distance': 'kinda-far'},
    {'code': '68A', 'distance': 'kinda-far'},
    {'code': '83', 'distance': 'kinda-far'},
    {'code': '83A', 'distance': 'kinda-far'},
    {'code': '122', 'distance': 'kinda-far'},
    {'code': '140', 'distance': 'kinda-far'},
    {'code': '142', 'distance': 'kinda-far'},
]

pub_transs = [
    {'name': 'Luas (Sandyford Line)', 'code': 'Green Line',
     'daft_id': '3', 'pub_trans_type': 'luas'},
    {'name': 'Luas (Tallaght Line)', 'code': 'Red Line',
     'daft_id': '2', 'pub_trans_type': 'luas'},
    {'name': 'Dart', 'code': 'dart', 'daft_id': '1', 'pub_trans_type': 'dart'},
    {'name': '1', 'code': '1', 'daft_id': '4', 'pub_trans_type': 'bus'},
    {'name': '2', 'code': '2', 'daft_id': '5', 'pub_trans_type': 'bus'},
    {'name': '3', 'code': '3', 'daft_id': '224', 'pub_trans_type': 'bus'},
    {'name': '4', 'code': '4', 'daft_id': '227', 'pub_trans_type': 'bus'},
    {'name': '5', 'code': '5', 'daft_id': '6', 'pub_trans_type': 'bus'},
    {'name': '7', 'code': '7', 'daft_id': '7', 'pub_trans_type': 'bus'},
    {'name': '7A', 'code': '7A', 'daft_id': '8', 'pub_trans_type': 'bus'},
    {'name': '7B', 'code': '7B', 'daft_id': '9', 'pub_trans_type': 'bus'},
    {'name': '7D', 'code': '7D', 'daft_id': '10', 'pub_trans_type': 'bus'},
    {'name': '7N', 'code': '7N', 'daft_id': '11', 'pub_trans_type': 'bus'},
    {'name': '8', 'code': '8', 'daft_id': '225', 'pub_trans_type': 'bus'},
    {'name': '9', 'code': '9', 'daft_id': '228', 'pub_trans_type': 'bus'},
    {'name': '10', 'code': '10', 'daft_id': '12', 'pub_trans_type': 'bus'},
    {'name': '10A', 'code': '10A', 'daft_id': '13', 'pub_trans_type': 'bus'},
    {'name': '11', 'code': '11', 'daft_id': '14', 'pub_trans_type': 'bus'},
    {'name': '11A', 'code': '11A', 'daft_id': '15', 'pub_trans_type': 'bus'},
    {'name': '11B', 'code': '11B', 'daft_id': '16', 'pub_trans_type': 'bus'},
    {'name': '13', 'code': '13', 'daft_id': '17', 'pub_trans_type': 'bus'},
    {'name': '13A', 'code': '13A', 'daft_id': '18', 'pub_trans_type': 'bus'},
    {'name': '13B', 'code': '13B', 'daft_id': '19', 'pub_trans_type': 'bus'},
    {'name': '14', 'code': '14', 'daft_id': '20', 'pub_trans_type': 'bus'},
    {'name': '15', 'code': '15', 'daft_id': '22', 'pub_trans_type': 'bus'},
    {'name': '15A', 'code': '15A', 'daft_id': '23', 'pub_trans_type': 'bus'},
    {'name': '15B', 'code': '15B', 'daft_id': '24', 'pub_trans_type': 'bus'},
    {'name': '15C', 'code': '15C', 'daft_id': '25', 'pub_trans_type': 'bus'},
    {'name': '15D', 'code': '15D', 'daft_id': '26', 'pub_trans_type': 'bus'},
    {'name': '15E', 'code': '15E', 'daft_id': '27', 'pub_trans_type': 'bus'},
    {'name': '15F', 'code': '15F', 'daft_id': '28', 'pub_trans_type': 'bus'},
    {'name': '15N', 'code': '15N', 'daft_id': '29', 'pub_trans_type': 'bus'},
    {'name': '15X', 'code': '15X', 'daft_id': '30', 'pub_trans_type': 'bus'},
    {'name': '16', 'code': '16', 'daft_id': '31', 'pub_trans_type': 'bus'},
    {'name': '17', 'code': '17', 'daft_id': '33', 'pub_trans_type': 'bus'},
    {'name': '17A', 'code': '17A', 'daft_id': '34', 'pub_trans_type': 'bus'},
    {'name': '18', 'code': '18', 'daft_id': '35', 'pub_trans_type': 'bus'},
    {'name': '19', 'code': '19', 'daft_id': '36', 'pub_trans_type': 'bus'},
    {'name': '19A', 'code': '19A', 'daft_id': '37', 'pub_trans_type': 'bus'},
    {'name': '20B', 'code': '20B', 'daft_id': '38', 'pub_trans_type': 'bus'},
    {'name': '25', 'code': '25', 'daft_id': '39', 'pub_trans_type': 'bus'},
    {'name': '25A', 'code': '25A', 'daft_id': '40', 'pub_trans_type': 'bus'},
    {'name': '25N', 'code': '25N', 'daft_id': '41', 'pub_trans_type': 'bus'},
    {'name': '25X', 'code': '25X', 'daft_id': '42', 'pub_trans_type': 'bus'},
    {'name': '26', 'code': '26', 'daft_id': '43', 'pub_trans_type': 'bus'},
    {'name': '27', 'code': '27', 'daft_id': '44', 'pub_trans_type': 'bus'},
    {'name': '27B', 'code': '27B', 'daft_id': '45', 'pub_trans_type': 'bus'},
    {'name': '27C', 'code': '27C', 'daft_id': '46', 'pub_trans_type': 'bus'},
    {'name': '27N', 'code': '27N', 'daft_id': '47', 'pub_trans_type': 'bus'},
    {'name': '27X', 'code': '27X', 'daft_id': '48', 'pub_trans_type': 'bus'},
    {'name': '29A', 'code': '29A', 'daft_id': '49', 'pub_trans_type': 'bus'},
    {'name': '29N', 'code': '29N', 'daft_id': '50', 'pub_trans_type': 'bus'},
    {'name': '31', 'code': '31', 'daft_id': '51', 'pub_trans_type': 'bus'},
    {'name': '31A', 'code': '31A', 'daft_id': '52', 'pub_trans_type': 'bus'},
    {'name': '31B', 'code': '31B', 'daft_id': '53', 'pub_trans_type': 'bus'},
    {'name': '31N', 'code': '31N', 'daft_id': '54', 'pub_trans_type': 'bus'},
    {'name': '32', 'code': '32', 'daft_id': '55', 'pub_trans_type': 'bus'},
    {'name': '32A', 'code': '32A', 'daft_id': '56', 'pub_trans_type': 'bus'},
    {'name': '32B', 'code': '32B', 'daft_id': '57', 'pub_trans_type': 'bus'},
    {'name': '32X', 'code': '32X', 'daft_id': '58', 'pub_trans_type': 'bus'},
    {'name': '33', 'code': '33', 'daft_id': '59', 'pub_trans_type': 'bus'},
    {'name': '33A', 'code': '33A', 'daft_id': '60', 'pub_trans_type': 'bus'},
    {'name': '33B', 'code': '33B', 'daft_id': '61', 'pub_trans_type': 'bus'},
    {'name': '33N', 'code': '33N', 'daft_id': '62', 'pub_trans_type': 'bus'},
    {'name': '37', 'code': '37', 'daft_id': '63', 'pub_trans_type': 'bus'},
    {'name': '37X', 'code': '37X', 'daft_id': '64', 'pub_trans_type': 'bus'},
    {'name': '38', 'code': '38', 'daft_id': '65', 'pub_trans_type': 'bus'},
    {'name': '38A', 'code': '38A', 'daft_id': '66', 'pub_trans_type': 'bus'},
    {'name': '38B', 'code': '38B', 'daft_id': '67', 'pub_trans_type': 'bus'},
    {'name': '38C', 'code': '38C', 'daft_id': '68', 'pub_trans_type': 'bus'},
    {'name': '39', 'code': '39', 'daft_id': '69', 'pub_trans_type': 'bus'},
    {'name': '39A', 'code': '39A', 'daft_id': '70', 'pub_trans_type': 'bus'},
    {'name': '39B', 'code': '39B', 'daft_id': '71', 'pub_trans_type': 'bus'},
    {'name': '39N', 'code': '39N', 'daft_id': '72', 'pub_trans_type': 'bus'},
    {'name': '39X', 'code': '39X', 'daft_id': '73', 'pub_trans_type': 'bus'},
    {'name': '40', 'code': '40', 'daft_id': '74', 'pub_trans_type': 'bus'},
    {'name': '40A', 'code': '40A', 'daft_id': '75', 'pub_trans_type': 'bus'},
    {'name': '40B', 'code': '40B', 'daft_id': '76', 'pub_trans_type': 'bus'},
    {'name': '40C', 'code': '40C', 'daft_id': '77', 'pub_trans_type': 'bus'},
    {'name': '40D', 'code': '40D', 'daft_id': '78', 'pub_trans_type': 'bus'},
    {'name': '40N', 'code': '40N', 'daft_id': '79', 'pub_trans_type': 'bus'},
    {'name': '41', 'code': '41', 'daft_id': '80', 'pub_trans_type': 'bus'},
    {'name': '41A', 'code': '41A', 'daft_id': '81', 'pub_trans_type': 'bus'},
    {'name': '41B', 'code': '41B', 'daft_id': '82', 'pub_trans_type': 'bus'},
    {'name': '41C', 'code': '41C', 'daft_id': '83', 'pub_trans_type': 'bus'},
    {'name': '41N', 'code': '41N', 'daft_id': '84', 'pub_trans_type': 'bus'},
    {'name': '41X', 'code': '41X', 'daft_id': '85', 'pub_trans_type': 'bus'},
    {'name': '42', 'code': '42', 'daft_id': '86', 'pub_trans_type': 'bus'},
    {'name': '42A', 'code': '42A', 'daft_id': '87', 'pub_trans_type': 'bus'},
    {'name': '42B', 'code': '42B', 'daft_id': '88', 'pub_trans_type': 'bus'},
    {'name': '42N', 'code': '42N', 'daft_id': '89', 'pub_trans_type': 'bus'},
    {'name': '43', 'code': '43', 'daft_id': '90', 'pub_trans_type': 'bus'},
    {'name': '44', 'code': '44', 'daft_id': '91', 'pub_trans_type': 'bus'},
    {'name': '44B', 'code': '44B', 'daft_id': '92', 'pub_trans_type': 'bus'},
    {'name': '44C', 'code': '44C', 'daft_id': '93', 'pub_trans_type': 'bus'},
    {'name': '44N', 'code': '44N', 'daft_id': '94', 'pub_trans_type': 'bus'},
    {'name': '45', 'code': '45', 'daft_id': '95', 'pub_trans_type': 'bus'},
    {'name': '45A', 'code': '45A', 'daft_id': '96', 'pub_trans_type': 'bus'},
    {'name': '46', 'code': '46', 'daft_id': '97', 'pub_trans_type': 'bus'},
    {'name': '46A', 'code': '46A', 'daft_id': '98', 'pub_trans_type': 'bus'},
    {'name': '46B', 'code': '46B', 'daft_id': '99', 'pub_trans_type': 'bus'},
    {'name': '46C', 'code': '46C', 'daft_id': '100', 'pub_trans_type': 'bus'},
    {'name': '46D', 'code': '46D', 'daft_id': '101', 'pub_trans_type': 'bus'},
    {'name': '46E', 'code': '46E', 'daft_id': '102', 'pub_trans_type': 'bus'},
    {'name': '46N', 'code': '46N', 'daft_id': '103', 'pub_trans_type': 'bus'},
    {'name': '46X', 'code': '46X', 'daft_id': '104', 'pub_trans_type': 'bus'},
    {'name': '48A', 'code': '48A', 'daft_id': '105', 'pub_trans_type': 'bus'},
    {'name': '48N', 'code': '48N', 'daft_id': '106', 'pub_trans_type': 'bus'},
    {'name': '49', 'code': '49', 'daft_id': '107', 'pub_trans_type': 'bus'},
    {'name': '49A', 'code': '49A', 'daft_id': '108', 'pub_trans_type': 'bus'},
    {'name': '49N', 'code': '49N', 'daft_id': '109', 'pub_trans_type': 'bus'},
    {'name': '49X', 'code': '49X', 'daft_id': '110', 'pub_trans_type': 'bus'},
    {'name': '50', 'code': '50', 'daft_id': '111', 'pub_trans_type': 'bus'},
    {'name': '50X', 'code': '50X', 'daft_id': '112', 'pub_trans_type': 'bus'},
    {'name': '51', 'code': '51', 'daft_id': '113', 'pub_trans_type': 'bus'},
    {'name': '51A', 'code': '51A', 'daft_id': '114', 'pub_trans_type': 'bus'},
    {'name': '51B', 'code': '51B', 'daft_id': '115', 'pub_trans_type': 'bus'},
    {'name': '51C', 'code': '51C', 'daft_id': '116', 'pub_trans_type': 'bus'},
    {'name': '51D', 'code': '51D', 'daft_id': '117', 'pub_trans_type': 'bus'},
    {'name': '51N', 'code': '51N', 'daft_id': '118', 'pub_trans_type': 'bus'},
    {'name': '51X', 'code': '51X', 'daft_id': '119', 'pub_trans_type': 'bus'},
    {'name': '53', 'code': '53', 'daft_id': '120', 'pub_trans_type': 'bus'},
    {'name': '53A', 'code': '53A', 'daft_id': '121', 'pub_trans_type': 'bus'},
    {'name': '54A', 'code': '54A', 'daft_id': '122', 'pub_trans_type': 'bus'},
    {'name': '54N', 'code': '54N', 'daft_id': '123', 'pub_trans_type': 'bus'},
    {'name': '56A', 'code': '56A', 'daft_id': '124', 'pub_trans_type': 'bus'},
    {'name': '58C', 'code': '58C', 'daft_id': '125', 'pub_trans_type': 'bus'},
    {'name': '58X', 'code': '58X', 'daft_id': '126', 'pub_trans_type': 'bus'},
    {'name': '59', 'code': '59', 'daft_id': '127', 'pub_trans_type': 'bus'},
    {'name': '63', 'code': '63', 'daft_id': '128', 'pub_trans_type': 'bus'},
    {'name': '65', 'code': '65', 'daft_id': '129', 'pub_trans_type': 'bus'},
    {'name': '65B', 'code': '65B', 'daft_id': '130', 'pub_trans_type': 'bus'},
    {'name': '65X', 'code': '65X', 'daft_id': '131', 'pub_trans_type': 'bus'},
    {'name': '66', 'code': '66', 'daft_id': '132', 'pub_trans_type': 'bus'},
    {'name': '66A', 'code': '66A', 'daft_id': '133', 'pub_trans_type': 'bus'},
    {'name': '66B', 'code': '66B', 'daft_id': '134', 'pub_trans_type': 'bus'},
    {'name': '66N', 'code': '66N', 'daft_id': '135', 'pub_trans_type': 'bus'},
    {'name': '66X', 'code': '66X', 'daft_id': '136', 'pub_trans_type': 'bus'},
    {'name': '67', 'code': '67', 'daft_id': '137', 'pub_trans_type': 'bus'},
    {'name': '67A', 'code': '67A', 'daft_id': '138', 'pub_trans_type': 'bus'},
    {'name': '67N', 'code': '67N', 'daft_id': '139', 'pub_trans_type': 'bus'},
    {'name': '67X', 'code': '67X', 'daft_id': '140', 'pub_trans_type': 'bus'},
    {'name': '68', 'code': '68', 'daft_id': '141', 'pub_trans_type': 'bus'},
    {'name': '69', 'code': '69', 'daft_id': '142', 'pub_trans_type': 'bus'},
    {'name': '69N', 'code': '69N', 'daft_id': '143', 'pub_trans_type': 'bus'},
    {'name': '69X', 'code': '69X', 'daft_id': '144', 'pub_trans_type': 'bus'},
    {'name': '70', 'code': '70', 'daft_id': '145', 'pub_trans_type': 'bus'},
    {'name': '70N', 'code': '70N', 'daft_id': '146', 'pub_trans_type': 'bus'},
    {'name': '70X', 'code': '70X', 'daft_id': '147', 'pub_trans_type': 'bus'},
    {'name': '75', 'code': '75', 'daft_id': '148', 'pub_trans_type': 'bus'},
    {'name': '76', 'code': '76', 'daft_id': '149', 'pub_trans_type': 'bus'},
    {'name': '76A', 'code': '76A', 'daft_id': '150', 'pub_trans_type': 'bus'},
    {'name': '76B', 'code': '76B', 'daft_id': '151', 'pub_trans_type': 'bus'},
    {'name': '77', 'code': '77', 'daft_id': '152', 'pub_trans_type': 'bus'},
    {'name': '77A', 'code': '77A', 'daft_id': '153', 'pub_trans_type': 'bus'},
    {'name': '77B', 'code': '77B', 'daft_id': '154', 'pub_trans_type': 'bus'},
    {'name': '77N', 'code': '77N', 'daft_id': '155', 'pub_trans_type': 'bus'},
    {'name': '77X', 'code': '77X', 'daft_id': '156', 'pub_trans_type': 'bus'},
    {'name': '78', 'code': '78', 'daft_id': '157', 'pub_trans_type': 'bus'},
    {'name': '78A', 'code': '78A', 'daft_id': '158', 'pub_trans_type': 'bus'},
    {'name': '79', 'code': '79', 'daft_id': '159', 'pub_trans_type': 'bus'},
    {'name': '83', 'code': '83', 'daft_id': '160', 'pub_trans_type': 'bus'},
    {'name': '84', 'code': '84', 'daft_id': '161', 'pub_trans_type': 'bus'},
    {'name': '84N', 'code': '84N', 'daft_id': '162', 'pub_trans_type': 'bus'},
    {'name': '84X', 'code': '84X', 'daft_id': '163', 'pub_trans_type': 'bus'},
    {'name': '86', 'code': '86', 'daft_id': '164', 'pub_trans_type': 'bus'},
    {'name': '88N', 'code': '88N', 'daft_id': '165', 'pub_trans_type': 'bus'},
    {'name': '90', 'code': '90', 'daft_id': '166', 'pub_trans_type': 'bus'},
    {'name': '90A', 'code': '90A', 'daft_id': '167', 'pub_trans_type': 'bus'},
    {'name': '102', 'code': '102', 'daft_id': '168', 'pub_trans_type': 'bus'},
    {'name': '103', 'code': '103', 'daft_id': '169', 'pub_trans_type': 'bus'},
    {'name': '104', 'code': '104', 'daft_id': '170', 'pub_trans_type': 'bus'},
    {'name': '105', 'code': '105', 'daft_id': '171', 'pub_trans_type': 'bus'},
    {'name': '111', 'code': '111', 'daft_id': '172', 'pub_trans_type': 'bus'},
    {'name': '114', 'code': '114', 'daft_id': '173', 'pub_trans_type': 'bus'},
    {'name': '115', 'code': '115', 'daft_id': '174', 'pub_trans_type': 'bus'},
    {'name': '116', 'code': '116', 'daft_id': '175', 'pub_trans_type': 'bus'},
    {'name': '117', 'code': '117', 'daft_id': '176', 'pub_trans_type': 'bus'},
    {'name': '118', 'code': '118', 'daft_id': '177', 'pub_trans_type': 'bus'},
    {'name': '120', 'code': '120', 'daft_id': '178', 'pub_trans_type': 'bus'},
    {'name': '121', 'code': '121', 'daft_id': '179', 'pub_trans_type': 'bus'},
    {'name': '122', 'code': '122', 'daft_id': '180', 'pub_trans_type': 'bus'},
    {'name': '123', 'code': '123', 'daft_id': '181', 'pub_trans_type': 'bus'},
    {'name': '127', 'code': '127', 'daft_id': '182', 'pub_trans_type': 'bus'},
    {'name': '129', 'code': '129', 'daft_id': '183', 'pub_trans_type': 'bus'},
    {'name': '130', 'code': '130', 'daft_id': '184', 'pub_trans_type': 'bus'},
    {'name': '140', 'code': '140', 'daft_id': '226', 'pub_trans_type': 'bus'},
    {'name': '145', 'code': '145', 'daft_id': '185', 'pub_trans_type': 'bus'},
    {'name': '146', 'code': '146', 'daft_id': '186', 'pub_trans_type': 'bus'},
    {'name': '150', 'code': '150', 'daft_id': '187', 'pub_trans_type': 'bus'},
    {'name': '161', 'code': '161', 'daft_id': '188', 'pub_trans_type': 'bus'},
    {'name': '172', 'code': '172', 'daft_id': '189', 'pub_trans_type': 'bus'},
    {'name': '184', 'code': '184', 'daft_id': '190', 'pub_trans_type': 'bus'},
    {'name': '185', 'code': '185', 'daft_id': '191', 'pub_trans_type': 'bus'},
    {'name': '201', 'code': '201', 'daft_id': '192', 'pub_trans_type': 'bus'},
    {'name': '202', 'code': '202', 'daft_id': '193', 'pub_trans_type': 'bus'},
    {'name': '206', 'code': '206', 'daft_id': '194', 'pub_trans_type': 'bus'},
    {'name': '210', 'code': '210', 'daft_id': '195', 'pub_trans_type': 'bus'},
    {'name': '220', 'code': '220', 'daft_id': '196', 'pub_trans_type': 'bus'},
    {'name': '230', 'code': '230', 'daft_id': '197', 'pub_trans_type': 'bus'},
    {'name': '236', 'code': '236', 'daft_id': '198', 'pub_trans_type': 'bus'},
    {'name': '237', 'code': '237', 'daft_id': '199', 'pub_trans_type': 'bus'},
    {'name': '238', 'code': '238', 'daft_id': '200', 'pub_trans_type': 'bus'},
    {'name': '239', 'code': '239', 'daft_id': '201', 'pub_trans_type': 'bus'},
    {'name': '270', 'code': '270', 'daft_id': '202', 'pub_trans_type': 'bus'},
    {'name': '746', 'code': '746', 'daft_id': '203', 'pub_trans_type': 'bus'},
    {'name': '747', 'code': '747', 'daft_id': '204', 'pub_trans_type': 'bus'},
    {'name': '748', 'code': '748', 'daft_id': '205', 'pub_trans_type': 'bus'},
]


xmls = [
    {
        'url': 'https://www.daft.ie/rss.daft?uid=1596514&id=986890&xk=424844',
        'min_price': 0,
        'max_price': 2000,
        'type': 'rent',
        'locations': 'Co. Dublin and Commuters',
        'description': '',
    },
    {
        'url': 'https://www.daft.ie/rss.daft?uid=1596514&id=989524&xk=96921',
        'min_price': 0,
        'max_price': 1500,
        'type': 'sharing',
        'locations': 'Co. Dublin and Commuters',
        'description': ('A single or double room in shared accommodation'
                        ' for less than €1,500 for a male '
                        'available for more than 5 months in Co. Dublin'),
    },
]


@celery.task(bind=True)
def add_users_bot(self):
    _return = []
    with current_app.app_context():
        for user in users:
            newrec = User(
                id=user['id'],
                username=user['user'],
                email=user['email'],
            )
            newrec.set_password(user['pass'])

            _return.append(str(db.session.merge(newrec)))
            db.session.commit()

        id = 0
        for xml in xmls:
            id += 1
            newrec = RSSAlert(
                id=id,
                url=xml['url'],
                type=xml['type'],
                locations=xml['locations'],
                min_price=xml['min_price'],
                max_price=xml['max_price'],
                description=xml['description'],
                user_id=1,
            )

            _return.append(str(db.session.merge(newrec)))
            db.session.commit()
    return _return


@celery.task(bind=True)
def add_routes(self):

    for pub_trans in pub_transs:
        cur_id = None
        cur = db.session.query(PubTrans).filter_by(
            pub_trans_code=pub_trans['code']
        ).first()
        if(cur):
            cur_id = cur.id

        newrec = PubTrans(
            id=cur_id,
            pub_trans_name=pub_trans['name'],
            pub_trans_code=pub_trans['code'],
            daft_id=pub_trans['daft_id'],
            pub_trans_type=pub_trans['pub_trans_type'],
        )

        db.session.merge(newrec)

    for viable_trans in viable_transs:
        _desc = 'startdb code: ' + viable_trans['code']
        cur_id = None
        cur = db.session.query(PubTransAlert).filter_by(
            desc=_desc
        ).first()
        if(cur):
            cur_id = cur.id

        newrec = PubTransAlert(
            id=cur_id,
            pub_trans=db.session.query(PubTrans).filter_by(
                pub_trans_code=viable_trans['code']).first(),
            user_id=1,
            distance=viable_trans['distance'],
            desc=_desc,
        )

        db.session.merge(newrec)

    db.session.commit()
