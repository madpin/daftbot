from flask import Blueprint

bp = Blueprint('api', __name__)

from daftbot.routes.api import routes
