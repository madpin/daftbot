from flask import current_app
from daftbot.routes.api import bp
from daftbot.email import send_email

from flask import jsonify, request, url_for


@bp.route('/celery_status', methods=['GET'])
def celery_status():
    from daftbot import celery
    # from daftbot import db
    ret = []
    i = celery.control.inspect()

    return jsonify({
        'i.scheduled()': i.scheduled(),
        'i.active()': i.active(),
        'i.reserved()': i.reserved(),
    })


@bp.route('/getweb', methods=['GET'])
def getweb():
    import requests
    r = requests.get('https://api.exchangeratesapi.io/latest?base=BRL',)
    return jsonify(r.json())


from daftbot.scripts.startdb import add_users_bot


@bp.route('/add_users', methods=['GET'])
def add_users():
    _data = str(add_users_bot.delay())
    return jsonify({'process': 'done', 'data': _data})


from daftbot.scripts.startdb import add_routes
from daftbot.scripts.celery import update_celery_task


@bp.route('/startdb', methods=['GET'])
def startdb():
    _data_add_users_bot = add_users_bot.delay()
    _data_add_routes = add_routes.delay()
    return jsonify({
        'process': 'done',
        'add_users_bot': str(_data_add_users_bot),
        'add_routes': str(_data_add_routes),
    })


from daftbot.scripts.listings import get_sharing_listings
from daftbot.scripts.daft2db import daft2db
from daftbot.scripts.listings import loop_new_listings
import pickle


@bp.route('/loop_daft', methods=['GET'])
def loop_daft():
    ret = []
    ret = loop_new_listings(max_listing_pages=999999)
    return jsonify({'code': ret})


from daftbot.scripts.listings import loop_xml_listings


@bp.route('/loop_xmls', methods=['GET'])
def loop_xmls():
    ret = []
    ret = loop_xml_listings()
    return jsonify({'code': ret})


from daftbot.scripts import here


@bp.route('/loop_here', methods=['GET'])
def loop_here():
    ret = []
    ret = here.loop_here()
    return jsonify({'code': ret})


from daftbot.scripts.download_images import loop_images


@bp.route('/download_images', methods=['GET'])
def download_images():
    ret = []
    ret = loop_images()
    return jsonify({'code': ret})

from daftbot.scripts.telegram import loop_telegram
@bp.route('/send_telegram', methods=['GET'])
def send_telegram():

    ret = []
    ret = loop_telegram()
    return jsonify({'code': ret})
