from flask import Blueprint

bp = Blueprint('main', __name__)

from daftbot.routes.main import routes
