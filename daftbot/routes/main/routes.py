from flask import (
    render_template, flash, redirect, url_for, request, g, current_app
)
from flask_login import current_user, login_required
from datetime import datetime

from daftbot import db
from daftbot.models import User, Listing
from daftbot.routes.main.forms import EditProfileForm, SearchForm
from daftbot.routes.main import bp


@bp.before_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.now()
        db.session.commit()
        g.search_form = SearchForm()


@bp.route('/')
@bp.route('/index')
def index():
    return render_template('index.html', title='Home')


@bp.route('/user/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    posts = [
        {'author': user, 'body': 'Test post #1'},
        {'author': user, 'body': 'Test post #2'}
    ]
    return render_template('user/user.html', user=user, posts=posts)


@bp.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm(current_user.username)
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.email = form.email.data
        current_user.about_me = form.about_me.data
        db.session.commit()
        flash('Your changes have been saved.')
        return redirect(url_for('main.edit_profile'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.email.data = current_user.email
        form.about_me.data = current_user.about_me
    return render_template('user/edit_profile.html', title='Edit Profile',
                           form=form)


@bp.route('/search')
@login_required
def search():
    return render_template('search.html', title='Search')
#     if not g.search_form.validate():
#         return redirect(url_for('main.explore'))
#     page = request.args.get('page', 1, type=int)
#     posts, total = Post.search(g.search_form.q.data, page,
#                                current_app.config['POSTS_PER_PAGE'])
#     next_url = url_for('main.search', q=g.search_form.q.data, page=page + 1) \
#         if total > page * current_app.config['POSTS_PER_PAGE'] else None
#     prev_url = url_for('main.search', q=g.search_form.q.data, page=page - 1) \
#         if page > 1 else None
#     return render_template('search.html', title=_('Search'), posts=posts,
#                            next_url=next_url, prev_url=prev_url)


@bp.route('/listings')
@login_required
def listings():
    page = request.args.get('page', 1, type=int)
    listings = db.session.query(
        Listing
    ).order_by(
        Listing.date_insert_update.desc()
    ).order_by(
        Listing.source_xml_pub_date.desc().nullslast()
    ).paginate(
        page, current_app.config['LISTINGS_PER_PAGE'], False)


    next_url = url_for('main.listings', page=listings.next_num) \
        if listings.has_next else None

    prev_url = url_for('main.listings', page=listings.prev_num) \
        if listings.has_prev else None

    return render_template('main/listings.html',
                           listings=listings.items,
                           next_url=next_url,
                           prev_url=prev_url)



@bp.route('/api')
def api():

    return render_template('main/api.html',)