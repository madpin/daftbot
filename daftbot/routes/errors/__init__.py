from flask import Blueprint

bp = Blueprint('errors', __name__)

from daftbot.routes.errors import handlers