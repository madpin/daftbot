from flask import current_app
from daftbot.routes.tests import bp
from daftbot.email import send_email
from daftbot import db

from flask import jsonify, request, url_for


@bp.route('/route_listing', methods=['GET'])
def route_listing():
    ret = []
    from daftbot.models import Listing, RouteToIndeed
    recs = db.session.query(
        Listing
    ).limit(25)

    for rec in recs:
        obj = {}
        obj['obj'] = str(rec)
        if(rec.routes_to_indeed):
            obj['routes_to_indeed'] = str(rec.routes_to_indeed)
        # for route in rec.routes_to_indeed:
        #     obj['routes_to_indeed'].append(route)
        ret.append(obj)

    # db.session.add(newrec)
    # db.session.commit()
    return jsonify(ret)

@bp.route('/route_to_indeed_creation', methods=['GET'])
def route_to_indeed_creation():

    from daftbot.models import PubTrans
    from daftbot.models import RouteToIndeed
    ptrans = PubTrans()
    ptrans.pub_trans_code = 'teste'

    newrec = RouteToIndeed(
        pub_trans=[ptrans],
        lines_count=1,
        description='teste123',
        latitude=-6,
        longitude=33
    )

    db.session.add(newrec)
    db.session.commit()
    return '1'


@bp.route('/route_to_indeed', methods=['GET'])
def route_to_indeed():
    import datetime
    from daftbot.scripts import here
    today = datetime.date.today()
    to_friday = 5+7-today.isoweekday()
    next2_friday = today + datetime.timedelta(days=to_friday)
    complete_date = datetime.datetime.combine(next2_friday,
                                              datetime.time(8, 45))
    return jsonify(here.get_routes_to_indeed(complete_date, (53.09588, -8.22160)))


@bp.route('/listing_dict', methods=['GET'])
def listing_dict():
    print('######### Entrando ######### ')
    from daftbot.models import PubTransAlert
    import daftlistings as daft
    from daftbot.scripts.listings import get_sharing_listings
    # ret = []
    print('######### f1 ######### ')
    pub_trans_alert = db.session.query(
        PubTransAlert
    ).filter(
        PubTransAlert.pub_trans_id.isnot(None)
    ).order_by(
        PubTransAlert.last_run.desc()
    ).order_by(
        PubTransAlert.current_finished.asc()
    ).first()

    print('######### f2 ######### ')
    listing_data = get_sharing_listings(
        pub_trans_alert.pub_trans.daft_id,
        0,
    )
    print('######### f3 ######### ')
    print(listing_data[0]._data_from_search)
    print('######### f4 ######### ')
    return str(listing_data[0]._data_from_search)
    # return jsonify(listing_data[0]._data_from_search)



from daftbot.models import CeleryTask
import string
import random


@bp.route('/task_test', methods=['GET'])
def task_test():
    newrec = CeleryTask(
        id='1',
        status=''.join(random.choices(
            string.ascii_lowercase + string.digits, k=20
        )),
    )
    db.session.merge(newrec)
    db.session.commit()

    rec = db.session.query(
        CeleryTask
    ).filter_by(
        id=1
    ).first()
    return jsonify({
        'updated': rec.updated_at.strftime('%Y-%d-%m %H:%M:%S'),
        'status': rec.status,
    })
