from flask import Blueprint

bp = Blueprint('tests', __name__)

from daftbot.routes.tests import routes
