from flask import Blueprint

bp = Blueprint('auth', __name__)

from daftbot.routes.auth import routes
