# -*- coding: utf-8 -*-
from flask import render_template, current_app
from flask_mail import Message
from daftbot import mail, celery
# from threading import Thread

@celery.task
def send_email(subject, sender, recipients, text_body, html_body):
    print('teste de print')
    msg = Message(subject, sender=sender, recipients=recipients)
    msg.body = text_body
    msg.html = html_body
    # Thread(target=send_async_email, args=(current_app, msg)).start()
    with current_app.app_context():
        mail.send(msg)

