import click
from flask.cli import AppGroup

bot_cli = AppGroup('bot')

@bot_cli.command('create')
@click.argument('name')
def create_user(name):
    print('Name: ', str(name))