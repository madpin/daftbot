#!/bin/sh
# source venv/bin/activate

# flask translate compile
# exec gunicorn -b :5000 --access-logfile - --error-logfile - run:app


case "$1" in
    gunicorn)
        # crond -f -l 8
        while true; do
            flask db upgrade
            if [[ "$?" == "0" ]]; then
                break
            fi
            echo Upgrade command failed, retrying in 5 secs...
            sleep 5
        done
        exec gunicorn -b :5000 --access-logfile - --error-logfile - run:app
    ;;
    debug)
        # crond -f -l 8
        while true; do
            echo flask db upgrade
            flask db upgrade
            if [[ "$?" == "0" ]]; then
                break
            fi
            echo Upgrade command failed, retrying in 5 secs...
            sleep 5
        done
        exec flask run --host=0.0.0.0
    ;;
    worker)
        exec celery worker -A celery_worker.celery --loglevel=debug --concurrency=4
    ;;
    worker_beat)
        exec celery beat -A celery_worker.celery --loglevel=debug
        # exec celery beat -A celery_worker.celery --loglevel=debug
    ;;
    *)
    # The command is something like bash, not an airflow subcommand. Just run it in the right environment.
    exec "$@"
    ;;
esac