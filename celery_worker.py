import os
import pickle
from daftbot import celery, create_app
# from daftbot import scripts.

app = create_app()
app.app_context().push()

class ContextTask(celery.Task):
    def __call__(self, *args, **kwargs):
        with app.app_context():
            return self.run(*args, **kwargs)

celery.Task = ContextTask
celery.conf.update(
    accept_content=['json', 'pickle'],  # Ignore other content
)
