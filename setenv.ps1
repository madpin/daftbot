
#  SETTHISVARIABLE=TOTHISVALUE
Get-Content "secrets.env" `
    | Select-String -Pattern "^[^#]\w*" `
    | ForEach-Object {$_.ToString().Replace("`"", "")} `
    | ConvertFrom-String -Delimiter "=" -PropertyNames envvar, envvarvalue `
    | ForEach-Object {[Environment]::SetEnvironmentVariable($_.envvar, $_.envvarvalue)}

$env:FLASK_APP = "run.py"
$env:FLASK_DEBUG = 1
$env:DATABASE_URL = "postgresql+psycopg2://postgres:passexample@192.168.99.100:5432/postgres"