Starting with:


First Tutorial Followed:
https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world

SQLAlchemy on Data
http://flask-sqlalchemy.pocoo.org/2.3/

Celery:
https://blog.miguelgrinberg.com/post/using-celery-with-flask

http://allynh.com/blog/flask-asynchronous-background-tasks-with-celery-and-redis/





NTKL:

http://www.nltk.org/howto/sentiment.html


## Deploying
```
mkdir -p /home/madpin/daftbot
mkdir -p /home/madpin/daftbot.git
cd /home/madpin/daftbot.git

git init --bare
cd hooks

cat >post-receive <<EOL
#!/bin/bash
TRAGET="/data/airflow/extensions"
GIT_DIR="/data/git.repos/airflow-extensions.git"
BRANCH="master"

while read oldrev newrev ref
do
    # only checking out the master (or whatever branch you would like to deploy)
    if [[ \$ref = refs/heads/\$BRANCH ]];
    then
        echo "Ref \$ref received. Deploying \${BRANCH} branch to production..."
        git --work-tree=\$TRAGET --git-dir=\$GIT_DIR checkout -f
    else
        echo "Ref $ref received. Doing nothing: only the \${BRANCH} branch may be deployed on this server."
    fi
done
EOL

chmod +x post-receive
```

Docker

docker logs daftbot

docker-compose build

docker-compose up

docker-compose exec -u 0 flask sh
docker-compose exec -u 0 celery_worker sh


docker-compose up --scale celery_worker=4


Meu chat id: 376547461